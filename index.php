<html>
<head>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<style>
.row{
padding-top: 100px;
    padding-left: 150px;
}

</style>
</head>
<body>
	<div id="wrapper">
		
		<div id="content">
		<div class="row">
		<h2>Enter URL</h2>
			<div class="col-md-6">
			
				<form method="post" action="process.php" name="lesnForm" id="lesnForm">
					<input type="text" name="url" id="url" value="" placeholder="http://" style="width: 100%;" />
					</div>
  <div class="col-md-4">
						<input type="submit" name="lesnBtn" class="btn btn-primary"></div>
				</form>
			</div>
			<h3 id="message" class="success"><?php echo ( isset($message) ? $message : '' );  ?></h3>
			
		</div>
	</div>	
	
	
	
<div class="container">
	<div class="row">
		
        <div class="col-md-12">
        
        <div class="table-responsive">
		<div class="panel panel-primary filterable">
		<div class="panel-heading">
               <h2>List of all Urls</h2>
                
            </div>
                
              <table id="mytable" class="table table-bordred table-striped">
                   
                   <thead>
					 <th>Sr.No.</th>
					 <th>Url</th>
					 <th>Short Urls</th>
                     <th>Short Code</th>
                      <th>Count</th>
					  <th>View</th>
					   
                   </thead>
    <tbody>
    <?php
       require_once("connect.php");
         $sqlq = "SELECT * FROM `short_urls` order by id desc";
         $q = mysqli_query($conn, $sqlq);
		// print_r($q);
		$i=1;
			while($test = mysqli_fetch_array($q))
			 //print_r($test);
			
    {        
	$id=$test['code']; 
	       
						
		   ?>
    <tr>    
    <td><?php echo $a= $i; ?></td>
	<td><?php echo $a= $test['url']; ?></td>
    <td><?php echo $a= $test['short_url']; ?></td>
    <td><?php echo $a= $test['code']; ?></td>
    <td><?php echo $a= $test['count']; ?></td>
	
    <td><a class="btn btn-info" href="view?nid=<?php echo $id ?>"><i class="glyphicon glyphicon-zoom-in icon-white"></i></a></td>
    <!--<td><a class="btn btn-danger" href="delete.php?nid=<?php echo $id ?>"><i class="glyphicon glyphicon-trash icon-white"></i></a></td>-->
    </tr>
  <?php 
  $i=$i+1;
   }
   
   ?>
    
    </tbody>
        
</table>

                
            </div>
            </div>
        </div>
	</div>
</div>
</body>

</html>